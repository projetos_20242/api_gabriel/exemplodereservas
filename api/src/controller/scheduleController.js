const connect = require("../db/connect");

module.exports = class scheduleControler {
  static async createSchedule(req, res) {
    const { dateStart, dateEnd, days, user, classroom, timeStart, timeEnd } =
      req.body;

    if (
      !dateStart ||
      !dateEnd ||
      !days ||
      !user ||
      !classroom ||
      !timeStart ||
      !timeEnd
    ) {
      return res
        .status(400)
        .json({ erro: "Todos os campos devem ser preenchidos" });
    }
    const daysString = days.map((day) => `${day}`.join(","));

    //verificando se já não existe uma reserva
    try {
      const overlapQuery = `SELECT * from schedule
            WHERE classroom = '${classroom}'
            AND(
              (dateStart <= '${dateEnd}'AND dateEnd >='${dateStart}')  
            ) AND(
                (timeStart <= '${timeEnd}'AND timeEnd >='${timeStart}')  
              ) AND(
                (days LIKE '%Seg%' AND '${daysString}' LIKE '%Seg%')OR
                (days LIKE '%Ter%' AND '${daysString}' LIKE '%Ter%')OR
                (days LIKE '%Qua%' AND '${daysString}' LIKE '%Qua%')OR
                (days LIKE '%Qui%' AND '${daysString}' LIKE '%Qui%')OR
                (days LIKE '%Sex%' AND '${daysString}' LIKE '%Sex%')OR
                (days LIKE '%Sab%' AND '${daysString}' LIKE '%Seg%')  
              ) 
            
            
            `;
      connect.query(overlapQuery, function (err, results) {
        if (err) {
          res
            .status(500)
            .json({ error: "Erro ao verificar agendamento existente" });
        }
        if (results.length > 0) {
          return res
            .status(400)
            .json({ error: "Sala ocupada para os mesmos dias e horários" });
        }
        const insertQuery = `
        INSERT INTO schedule (
            dateStart ,
            dateEnd ,
            days ,
            user ,
            classroom ,
            timeStart, 
            timeEnd
        )
        VALUES(
            '${dateStart}',
            '${dateEnd}' ,
            '${days}',
            '${user}' ,
            '${classroom}' ,
            '${timeStart}', 
            '${timeEnd}',
        )
        `;
        //executando a query de inserção
        connect.query(insertQuery, function (err){
            if(err) {
                return res.status(500).json({ error: "Erro ao cadastrar agendamento"});
            }
            return res.status(201).json({message: "Agendamento cadastrado com sucesso"});
        });
      });
    } catch (error) {

        console.error("Erro ao executar a consulta:", error);
        res.status(500).json({error: "Erro interno de servidor"})
    }
  }
};
